#include <libnotify/notify.h>

int main(int argc, char *argv[]) {

	if (argc == 1) {
		fprintf(stderr, "ERROR: %s\n", "Undefined App Name");
		return 1;
	}

	const char* appName = argv[1];
	const char* notificationTitle = (argc > 2) ? argv[2] : "";
	const char* notificationContent = (argc > 3) ? argv[3] : "";
	const char* notificationIcon = (argc > 4) ? argv[4] : "";

	notify_init(appName);

	NotifyNotification *notification = notify_notification_new(notificationTitle, notificationContent, notificationIcon);
	notify_notification_show(notification, NULL);
	g_object_unref(G_OBJECT(notification));
	notify_uninit();
	return 0;
}