# Notifications

Proyecto escrito en c que muestra notificaciones por entrada de argumentos
## Build

```console
user@host:~$ gcc -o notify $(pkgconf --cflags --libs libnotify) main.c
```

## Example Usage

```console
user@host:~$ ./notify "App Name" "Notification Title" "Notification Content" "path_to_image"
```

## Screenshot

![Notification](https://gitlab.com/Bermudez0622/notifications/-/raw/master/screenshot)